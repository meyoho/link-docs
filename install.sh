#!/bin/bash

SASS=$(which sass)
if [ "$?" != "0" ]; then
    echo "will install sass...."
    sudo gem install sass
else
    echo "sass already installed..."
fi

BREW=$(which brew)
if [ "$?" != "0" ]; then
    echo "will install brew...."
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
else
    echo "brew already installed..."
fi

HUGO=$(which hugo)
if [ "$?" != "0" ]; then
    echo "will install hugo..."
    brew install hugo
else
    echo "hugo already installed..."
fi;

GOVERSION=$(which go)
if [ "$?" != "0" ]; then
    echo "will install go..."
    brew install golang 
else
    echo "go already installed..."
fi;

LINKCRAWLER=$(which linkcrawler)
if [ "$?" != "0" ]; then
    go get -u github.com/danielfbm/linkcrawler
else
    echo "linkcrawler already installed..."
fi;

WKHTMLTOPDF=$(which wkhtmltopdf)
if [ "$?" != "0" ]; then
    echo "will download wkhtmltopdf and open its installer"
    curl -L -O https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.4/wkhtmltox-0.12.4_osx-cocoa-x86-64.pkg
    open wkhtmltox-0.12.4_osx-cocoa-x86-64.pkg
else
    echo "wkhtmltopdf already installed..."
fi;
echo "install complete!"

## merge pdf
## https://stackoverflow.com/questions/2507766/merge-convert-multiple-pdf-files-into-one-pdf#11280219