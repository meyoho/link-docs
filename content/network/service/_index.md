+++
title = "服务"
Description = "服务 (Service) 是一组 Pods 的逻辑集合，支持设定访问策略，可以看作是一个微服务。"
weight = 2
+++

服务 (Service) 是一组 Pods 的逻辑集合，支持设定访问策略，可以看作是一个微服务。Pods 通常是通过 Label Selector 被服务访问的。

对于 Kubernetes 的原生应用，Kubernetes 提供了一个简单的 Endpoints API，其作用是当一个服务中的 Pod 发生变化时，Endpoints API 也随之变化；对于非原生的程序，Kubernetes 提供了一个基于虚拟 IP 的网桥服务，这个服务会将请求重新定向到对应的 backend Pod。

{{%children style="card" description="true" %}}

