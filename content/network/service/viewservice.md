+++
title = "查看服务详情"
Description = "查看已创建的服务详情，例如：名称、类型、集群 IP、端点、容器组、YAML 等。"
weight = 2
+++

查看已创建的服务详情，例如：名称、类型、集群 IP、端点、容器组、YAML 等。

**操作步骤**

1. 登录 Kubernetes 平台，单击 **网络** > **服务**。

2. 在服务列表页面，选择服务所在的命名空间。

2. ​在服务列表页面，以列表的形式展示了所有已创建的服务，并以名称、类型、集群 IP、内部端点、选择器、创建时间来呈现。  
	
	* **名称**：已创建服务的名称。

	* **类型**：服务的类型。
	
		* **ClusterIP**：默认类型，为 Kubernetes Service 分配一个集群 IP（ClusterIP），仅用于集群内部的服务发现。
		
		* **NodePort**：在各个主机上开放一个固定端口（NodePort），通过该端口对外暴露服务，集群外部可通过 `<NodeIP>:<NodePort>` 访问该服务。同时分配一个集群 IP，提供内部服务发现。

		* **LoadBalancer**：在 NodePort 和 ClusterIP 类型基础上实现。通过引入外部负载均衡器，将外部访问流量转发到通过服务暴露的 `<NodeIP>:<NodePort>` 上。

		* **ExternalName**：引入外部服务到 Kubernetes 集群中，通过 cname 解析方式，将服务映射到指定的外部域名（ExternalName，例如`foo.example.com`）。
		
	* **集群 IP**：即 ClusterIP，用于集群内部的服务发现和访问。当服务类型为 **ExternalName** 时，无此配置信息。

	* **内部端点**：即 Internal Endpoints，显示服务在集群内部暴露的所有访问端口信息。例如：`nginx:80 TCP`。当服务类型为 **ExternalName** 时，无此配置信息。

	* **选择器**：通过匹配标签来识别与服务相关联的资源，例如容器组。当服务类型为 **ExternalName** 时，无此配置信息。
	
	* **创建时间**：服务的创建时间。
	
	* **操作**：单击 ![operations](/img/operations.png)，查看服务支持的操作功能。

3. 按名称搜索要查看的服务，找到后，单击 ***服务名称***。

4. 在服务详情页面，在 **基本信息** 栏，查看服务的基本信息。部分参数解释如下。  			
	* **标签**：标签以 key/value 键值对的形式附加到各种对象上，例如 Pod、Node 等。Label 定义了对象的可识别属性，便于管理和选择。单击 ![pen](/img/pen.png)，更新标签。
				
	* **注解**：注解使用 key/value 键值对的形式，可以附加任意定义的信息，便于外部工具进行查找。单击 ![pen](/img/pen.png)，更新注解。

	* **选择器**：通过匹配标签来识别与服务相关联的资源，例如容器组。当服务类型为 **ExternalName** 时，无此配置信息。
		
	* **会话保持**：即 sessionAffinity，服务的会话保持配置。当服务类型为 **ExternalName** 时，无此配置信息。
		
		* **None**：不启用会话保持，默认选项。
		
		* **ClientIP**：启用会话保持。在会话保持时限内，服务根据访问者的 IP，把访问流量转发给固定的同一后端 Pod 处理。

	* **外部端点**：即 External Endpoints，如果存在服务的后端在集群外部的情况，显示此参数信息。
	
	* **负载均衡器IP**：外部负载均衡器的访问 IP 地址。当服务类型为 **LoadBalancer** 时，显示此参数信息。

	* **ExternalName**：服务映射的指定域名。当服务类型为 **ExternalName** 时，显示此参数信息。

6. 在 **端点** 栏，查看服务暴露的端点信息。当服务类型为 **ExternalName** 时，无此配置信息。

	* **名称**：端口配置的名称（name）。

	* **协议**：服务端口的协议类型（protocol）。
		
	* **服务端口**：服务在集群内部暴露的服务端口号（port）。
		
	* **Pod 端口**：服务端口映射的目标端口号（targetPort），该端口号为后端 Pod 对外暴露业务访问入口的端口号。
		
	* **主机端口**：在集群节点主机上，映射到服务端口的开放端口号（NodePort）。当服务类型为 **NodePort** 或 **LoadBalancer** 时，显示此信息。

7. 在 **容器组** 栏，显示了和服务相关的容器组信息，例如：名称、状态、已使用的 CPU 和内存资源、重启次数、可允许的操作等。单击 ***容器组名称***，进入到对应的详情页面。参考[查看容器组详情]({{< relref "workloads/pod/viewpod.md" >}})。

8. 在 **YAML** 栏，查看只读的 YAML 内容。  
	
	* 单击 **导出**，将 YAML 导出保存成一个文件。
	
	* 单击 **查找**，在框中输入关键字，会自动匹配搜索结果，并支持前后搜索查看。  
		
	* 单击 **复制**，复制编辑内容。  
		
	* 单击 **日间** 或 **夜间** 模式，屏幕会自动调节成对应的查看模式。 
		 
	* 单击 **全屏**，全屏查看内容；单击 **退出全屏**，退出全屏模式。
