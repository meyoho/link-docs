+++
title = "访问权"
Description = "访问权 (Ingress) 用来管理集群外部对集群内部服务的访问请求，Ingress 是为进入集群的请求提供路由规则的集合。"
weight = 2
+++

访问权 (Ingress) 用来管理集群外部对集群内部服务的访问请求，Ingress 是为进入集群的请求提供路由规则的集合。

访问权为服务提供集群外部访问的 URL、负载均衡、SSL 终止、HTTP 路由等。访问权通过 Ingress Controller 监听访问权和服务的变化，并根据规则配置负载均衡，提供访问入口。

{{%children style="card" description="true" %}}

