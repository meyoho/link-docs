+++
title = "网络"
description = "网络支持创建 Service，实现对容器中应用的配置管理；支持创建 Ingress，保存敏感信息，使保存数据更安全灵活。"
weight = 4
+++

网络支持：

* 创建服务 (Service)，为一个 Pod 逻辑集合设置集群内部访问方式，通常应用于集群内部的服务发现和访问。 

* 创建访问权 (Ingress)，为 Service 提供集群外部访问的 URL、负载均衡、SSL终止、HTTP路由等。

本模块包括以下主要功能：

* [服务]({{< relref "/network/service/_index.md" >}})

* [访问权]({{< relref "/network/ingress/_index.md" >}})
