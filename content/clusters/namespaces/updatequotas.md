+++
title = "更新资源配额"
Description = "更新已添加的资源配额（ResourceQuota）。"
weight = 7
+++

更新已添加的资源配额（ResourceQuota）。

**操作步骤**

1. 登录 Kubernetes 平台，单击 **集群** > **命名空间**。

2. 单击要更新资源配额的 ***命名空间名称***，进入命名空间详情页面。

3. 在资源配额区段的选择框中，选择要更新的资源配额，单击![operations](/img/operationblue.png)，再单击 **更新**。

4. 在更新页面，更新表单或 YAML，参考[添加资源配额]({{< relref "clusters/namespaces/createquotas.md" >}})。

5. 配置完成后，单击 **更新**。