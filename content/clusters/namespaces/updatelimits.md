+++
title = "更新资源限额"
Description = "更新已添加的资源限额（LimitRange）。"
weight = 10
+++

更新已添加的资源限额（LimitRange）。

**操作步骤**

1. 登录 Kubernetes 平台，单击 **集群** > **命名空间**。

2. 单击要更新资源限额的 ***命名空间名称***，进入命名空间详情页面。

3. 在资源限额区段的选择框中，选择要更新的资源限额，单击![operations](/img/operationblue.png)，再单击 **更新**。

4. 在更新页面，更新表单或 YAML，参考[添加资源限额]({{< relref "clusters/namespaces/createlimits.md" >}})。

5. 配置完成后，单击 **更新**。