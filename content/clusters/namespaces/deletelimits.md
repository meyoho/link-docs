+++
title = "删除资源限额"
Description = "删除已添加的资源限额（LimitRange）。"
weight = 11
+++

删除已添加的资源限额（LimitRange）。

**操作步骤**

1. 登录 Kubernetes 平台，单击 **集群** > **命名空间**。

2. 单击要删除资源限额的 ***命名空间名称***，进入命名空间详情页面。

3. 在资源限额区段的选择框中，选择要删除的资源限额，单击![operations](/img/operationblue.png)，再单击 **删除**。

4. 在新窗口中，单击 **确定**。