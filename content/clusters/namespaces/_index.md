+++
title = "命名空间"
Description = ""
weight = 2
+++

Namespace 是一个独立的命名空间，创建多个 namespace 相当于在集群中创建多个“虚拟集群”，每个 namespace 可以配置集群内资源配额，并与其它 namespace 的资源隔离。使用者可根据实际需求，例如根据项目属性（生产、测试和开发等）在 Kubernetes 集群中创建多个 namespace 进行资源的分配。

{{%children style="card" description="true" %}}

