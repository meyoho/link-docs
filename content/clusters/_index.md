+++
title = "集群"
description = "集群支持添加 Node 和创建 Namespace 。"
weight = 6
+++

* 集群支持添加 Node 作为 Pod 运行的主机，Node 可以是物理服务器或虚拟机。

* 支持创建 Namespace，创建多个 Namespace 相当于在集群中创建多个“虚拟集群”，每个 Namespace 可以配置集群内资源配额，并与其它 Namespace 的资源隔离，实现集群资源的分配。

本模块包括以下主要功能：

* [节点]({{< relref "clusters/nodes/_index.md" >}})

* [命名空间]({{< relref "clusters/namespaces/_index.md" >}})
