+++
title = "更新 Taint"
Description = "当使用者希望创建的 Pod 不被调度到某些节点上时，可以使用 Taint（污点），确保 Pod 不会被调度到这些节点上。通过更新节点设置的 Taint，实现对 Pod 的调度进行管理。"
weight = 6
+++

当使用者希望创建的 Pod 不被调度到某些节点上时，可以使用 Taint（污点），确保 Pod 不会被调度到这些节点上。通过更新节点设置的 Taint，实现对 Pod 的调度进行管理。

**操作步骤**

1. 登录 Kubernetes 平台，单击 **集群** > **节点**。

2. 在节点列表页面，可以通过以下两种方式更新 Taint：  
	* 找到要更新 Taint 的节点名称，单击 ![operations](/img/operations.png)，再单击 **更新 Taint**。

	* 单击更新 Taint 的 ***节点名称***，在详情页面，单击 **操作** > **更新 Taint**。

3. 在 **更新 Taint** 窗口，更新 **键**、**值** 和 **状态**，其中 **键** 和 **状态** 为必填项。支持以下三种状态。
	
	* **NoSchedule**：表示不允许调度，除非 Pod 配置了容忍 （Tolerations）。已调度的不受影响。

	* **PreferNoSchedule**：表示尽量不调度。相比 NoSchedule，PreferNoSchedule 在功能作用上一样，PreferNoSchedule 不是强制执行，是参考执行。

	* **NoExecute**：表示不允许运行。已运行的 Pod 会被强制调离到其他满足容忍条件的节点上。如果配置了 NoExecute，可以选择是否配置 tolerationSeconds 参数（定义在 Tolerations 上），如有配置，Pod 会在 tolerationSeconds 时间过后进行调离。

4. 单击 ![addkey](/img/addkey.png)，添加多组 Taint；或单击 ![operations](/img/minus.png)，删除已添加的 Taint。

4. 单击 **更新**。在节点的详情页面的 **基本信息** 区域，Taint 已更新。
