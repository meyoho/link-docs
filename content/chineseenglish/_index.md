+++
title = "中英文平台词汇对照表"
description = "查看 AKS 平台界面上，常用概念的中英文词汇对照表。"
weight = 10
+++

AKS 平台界面上，常用概念的中英文词汇对照如下表所述。以中文词汇的拼音首字母排序。

<style>table th:first-of-type { width: 150px;}</style> 

<style>table th:nth-of-type(2) { width: 150px;}</style>

<style>table { max-width: 330px;}</style> 

<style>thead { display: none;}</style>

<style>h2,h3 { margin: 0.1em 0 0.1em 0;}</style>

### B
|中文名称|英文名称|
|:---:|:---:|
|保密字典|Secret|
|标签|label|
|部署|Deployment|

### C
|中文名称|英文名称|
|:---:|:---:|
|持久卷|PV，PersistentVolume|
|持久卷声明|PVC，PersistentVolumeClaim|
|存储卷|Volume|
|存储类|StorageClass|

### D
|中文名称|英文名称|
|:---:|:---:|
|定时任务|CronJob|

### F
|中文名称|英文名称|
|:---:|:---:|
|访问权|Ingress|
|服务|Service|
|副本集|ReplicaSet|

### J
|中文名称|英文名称|
|:---:|:---:|
|集群|Cluster|
|计算|Workloads|

### M
|中文名称|英文名称|
|:---:|:---:|
|命名空间|Namespace|

### P
|中文名称|英文名称|
|:---:|:---:|
|配置|Config|
|配置字典|ConfigMap|

### R
|中文名称|英文名称|
|:---:|:---:|
|任务|Job|
|容器组|Pod|


### S
|中文名称|英文名称|
|:---:|:---:|
|事件|Event|
|守护进程集|DaemonSet|

### W
|中文名称|英文名称|
|:---:|:---:|
|网络策略|NetworkPolicy|

### Y
|中文名称|英文名称|
|:---:|:---:|
|有状态副本集|StatefulSet|

### Z
|中文名称|英文名称|
|:---:|:---:|
|注解|Annotation|
|资源配额|ResourceQuota|
|资源限额|LimitRange|
