+++
title = "持久卷"
Description = ""
weight = 2
+++

PersistentVolume (PV) 是集群中的一块网络存储，属于集群中的资源。PV 和存储卷功能类似，但独立于 Pod 的生命周期。

PV 提供存储资源。PV 和 PVC 将 Pod 和存储卷解耦，提供了方便的持久化存储。

{{%children style="card" description="true" %}}

