+++
title = "删除持久卷"
Description = "删除已创建的持久卷。当绑定的 PV 被删除后，绑定的 PVC 状态显示为匹配失败。"
weight = 6
+++

删除已创建的持久卷。当绑定的 PV 被删除后，绑定的 PVC 状态显示为匹配失败。

**操作步骤**

1. 登录 Kubernetes 平台，单击 **存储** > **持久卷**。

3. 在持久卷列表页面，可以通过以下两种方式删除持久卷：  
	* 找到要删除的持久卷名称，单击 ![operations](/img/operations.png)，再单击 **删除**。

	* 单击要删除的 ***持久卷名称***，在详情页面，单击 **操作** > **删除**。
4. 在新窗口，单击 **确定**。