+++
title = "存储"
description = "存储支持创建 PV，为 Pod 提供持久的存储资源，存储数据；支持创建 PVC，请求使用 PV 资源；支持创建 StorageClass，实现按需动态创建和调整存储资源，封装不同类型的存储供 PVC 使用。"
weight = 5
+++

存储支持：

* 创建持久卷声明 (PVC)，请求使用 PV 资源

* 创建持久卷 (PV)，为 Pod 提供持久的存储资源，存储数据

* 创建存储类 (StorageClass)，实现按需动态创建和调整存储资源，封装不同类型的存储供 PVC 使用

本模块包括以下主要功能：

* [持久卷声明]({{< relref "storage/pvc/_index.md" >}})

* [持久卷]({{< relref "storage/pv/_index.md" >}})

* [存储类]({{< relref "storage/storageclass/_index.md" >}})
