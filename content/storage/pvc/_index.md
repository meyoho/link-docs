+++
title = "持久卷声明"
Description = ""
weight = 1
+++

PersistentVolumeClaim (PVC，持久卷声明) 是对 PV 的使用请求，请求特定大小和访问模式的 PV。

PV 和 PVC 将 Pod 和存储卷解耦，提供了方便的持久化存储，PV 提供存储资源，PVC 请求访问存储资源。

{{%children style="card" description="true" %}}

