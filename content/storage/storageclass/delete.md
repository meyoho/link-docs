+++
title = "删除存储类"
Description = "删除已创建的存储类。"
weight = 8
+++

删除已创建的存储类。

**操作步骤**

1. 登录 Kubernetes 平台，单击 **存储** > **存储类**。

3. 在存储类列表页面，可以通过以下两种方式删除存储类：  
	* 找到要删除的存储类名称，单击 ![operations](/img/operations.png)，再单击 **删除**。

	* 单击要删除的 ***存储类名称***，在详情页面，单击 **操作** > **删除**。
4. 在新窗口，单击 **确定**。