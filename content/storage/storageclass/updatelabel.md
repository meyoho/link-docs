+++
title = "更新标签"
Description = "为存储类添加新标签，用于识别此存储类；或删除已添加的标签。"
weight = 6
+++

为存储类添加新标签，用于识别此存储类；或删除已添加的标签。

**操作步骤**

1. 登录 Kubernetes 平台，单击 **存储** > **存储类**。

3. 在存储类列表页面，可以通过以下两种方式更新标签：  
	* 找到要更新标签的存储类名称，单击 ![operations](/img/operations.png)，再单击 **更新标签**。

	* 单击要更新标签的 ***存储类名称***，在详情页面，在基本信息区域，单击 **标签** 后面的 ![pen](/img/pen.png)。
4. 在 **更新标签** 窗口，在框中输入标签的键和值，用于识别此存储类。单击 **添加**，增加更多标签。    
键的格式支持英文字母、数字 0 ~ 9、中横线 (-)、下划线 (_) 和点 (.)，并以字母或数字开头和结尾。字符数：1 ~ 63 个。
5. 在已添加的标签后面，单击 ![operations](/img/minus.png)，删除已添加的标签。
6. 单击 **更新**，在存储类的详情页面的 **基本信息** 区域，标签已添加或删除。