+++
title = "定时任务"
Description = "定时任务 (CronJob) 用来在指定的时间周期内运行指定的任务，类似于 Linux 系统的 crontab。"
weight = 4
+++

定时任务 (CronJob) 用来在指定的时间周期内运行指定的任务，类似于 Linux 系统的 crontab。

通过自动触发功能，任务可以在指定时间内周期性运行。

{{%children style="card" description="true" %}}

