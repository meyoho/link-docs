+++
title = "部署"
Description = "使用 Deployment 资源，为 Pod 和 ReplicaSets 提供声明式更新，只用于部署服务。"
weight = 1
+++

使用 Deployment 资源，为 Pod 和 ReplicaSets 提供声明式更新。只需要在 Deployment 中描述目标状态，Deployment controller 就会将实际状态改变到目标状态。

Deployment 适用于对网络存储状态没有特殊要求的无状态工作负载。例如：挂载 Volume 的 Pod 不再工作后，与 Deployment 关联的 ReplicaSet 会再启动一个可用的 Pod 来保证可用性，而新启动的 Pod 和之前的 Pod 没有关系。

{{%children style="card" description="true" %}}
