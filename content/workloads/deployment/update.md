+++
title = "更新部署"
Description = "更新已创建的部署。支持表单和 YAML 的方式更新。"
weight = 3
+++

更新已创建的部署。支持表单和 YAML 的方式更新。

**操作步骤**

1. 登录 Kubernetes 平台，单击 **计算** > **部署**。

2. 在部署列表页面，选择部署所在的命名空间。

3. ​在部署列表页面，可以通过以下两种方式更新部署：  
	
	* 找到要更新的部署名称，单击 ![operations](/img/operations.png)，再单击 **更新**。

	* 单击要更新的 ***部署名称***，在详情页面，单击 **操作** > **更新**。

4. 在更新页面，更新部署的信息。参考[用表单创建部署]({{< relref "workloads/deployment/createdeployment/createdeploymentform.md" >}})和[用 YAML 创建部署]({{< relref "workloads/deployment/createdeployment/createdeploymentyaml.md" >}})。

5. 编辑好内容后，单击 **更新**。