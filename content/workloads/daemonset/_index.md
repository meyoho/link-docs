+++
title = "守护进程集"
Description = "DaemonSet 在每个可用的计算节点上都运行一个容器副本，常用来部署集群的日志、监控或系统管理应用。"
weight = 2
+++

DaemonSet 在每个可用的计算节点上都运行一个容器副本，常用来部署集群的日志、监控或系统管理应用。实例数量一般与主机数量相等。典型的应用包括：日志收集、系统监控、系统程序等。

{{%children style="card" description="true" %}}

