+++
title = "创建守护进程集"
Description = "创建 DaemonSet 资源，在每个可用的计算节点上都运行一个容器副本，用来部署集群的日志、监控或系统管理应用。"
weight = 1
+++

创建 DaemonSet 资源，在每个可用的计算节点上都运行一个容器副本，用来部署集群的日志、监控或系统管理应用。

{{%children style="card" description="true" %}}