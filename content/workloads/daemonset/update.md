+++
title = "更新守护进程集"
Description = "更新已创建的守护进程集。支持表单和 YAML 的方式更新。"
weight = 3
+++

更新已创建的守护进程集。支持表单和 YAML 的方式更新。

**操作步骤**

1. 登录 Kubernetes 平台，单击 **计算** > **守护进程集**。

2. 在守护进程集列表页面，选择守护进程集所在的命名空间。

3. ​在守护进程集列表页面，可以通过以下两种方式更新守护进程集：  
	
	* 找到要更新的守护进程集名称，单击 ![operations](/img/operations.png)，再单击 **更新**。

	* 单击要更新的 ***守护进程集名称***，在详情页面，单击 **操作** > **更新**。

4. 在更新页面，更新部署的信息。参考[用表单创建守护进程集]({{< relref "workloads/daemonset/createdaemonset/createdaemonsetform.md" >}})和[用 YAML 创建守护进程集]({{< relref "workloads/daemonset/createdaemonset/createdaemonsetyaml.md" >}})。

5. 编辑好内容后，单击 **更新**。