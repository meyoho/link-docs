+++
title = "更新有状态副本集"
Description = "更新已创建的有状态副本集。支持表单和 YAML 的方式更新。"
weight = 3
+++

更新已创建的有状态副本集。支持表单和 YAML 的方式更新。

**操作步骤**

1. 登录 Kubernetes 平台，单击 **计算** > **有状态副本集**。

2. 在有状态副本集列表页面，选择有状态副本集所在的命名空间。

2. ​在有状态副本集列表页面，可以通过以下两种方式更新有状态副本集：  
	
	* 找到要更新的有状态副本集名称，单击 ![operations](/img/operations.png)，再单击 **更新**。

	* 单击要更新的 ***有状态副本集名称***，在详情页面，单击 **操作** > **更新**。

4. 在更新页面，更新部署的信息。参考[用表单创建有状态副本集]({{< relref "workloads/stateful/createstateful/createstatefulform.md" >}})和[用 YAML 创建有状态副本集]({{< relref "workloads/stateful/createstateful/createstatefulyaml.md" >}})。

5. 编辑好 YAML 内容后，单击 **更新**。