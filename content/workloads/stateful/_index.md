+++
title = "有状态副本集"
Description = "使用 StatefulSet 资源，为 Pod 提供唯一标识，保证部署和扩缩容的顺序，适用于对网络存储状态有特殊要求的工作负载。"
weight = 3
+++

使用 StatefulSet 资源，为 Pod 提供唯一标识，保证部署和扩缩容的顺序，适用于对网络存储状态有特殊要求的工作负载。例如：实现网络的唯一标识、稳定的持久化存储、有序部署和扩展。

{{%children style="card" description="true" %}}

  
