+++
title = "删除有状态副本集"
Description = "删除已创建的有状态副本集。"
weight = 9
+++

删除已创建的有状态副本集。

**操作步骤**

1. 登录 Kubernetes 平台，单击 **计算** > **有状态副本集**。

2. 在有状态副本集列表页面，选择有状态副本集所在的命名空间。

3. 在有状态副本集列表页面，可以通过以下两种方式删除有状态副本集：  
	* 找到要删除的有状态副本集名称，单击 ![operations](/img/operations.png)，再单击 **删除**。

	* 单击要删除的 ***有状态副本集名称***，在详情页面，单击 **操作** > **删除**。
4. 在新窗口，单击 **确定**。