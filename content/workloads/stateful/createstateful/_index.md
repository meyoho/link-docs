+++
title = "创建有状态副本集"
Description = "创建 Deployment 资源，为 Pod 和 ReplicaSets 提供声明式更新，适用于对网络存储状态没有特殊要求的无状态工作负载。"
weight = 1
+++

创建 Deployment 资源，为 Pod 和 ReplicaSets 提供声明式更新，适用于对网络存储状态没有特殊要求的无状态工作负载。

{{%children style="card" description="true" %}}