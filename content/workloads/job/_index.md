+++
title = "任务"
Description = "任务 (Jobs) 用来批量处理短暂的一次性任务，即仅执行一次的任务。Jobs 保证批处理任务的一个或多个 Pods 成功结束。"
weight = 5
+++

任务 (Jobs) 用来批量处理短暂的一次性任务，即仅执行一次的任务。Jobs 保证批处理任务的一个或多个 Pods 成功结束。当任务被删除时，在任务中创建的 Pods 也被清除。

Kubernetes 支持以下几种任务：

* 非并行任务：创建一个 Pod 直至成功结束。

* 固定结束次数的任务：设置 `.spec.completions` 参数，创建多个 Pods，直到 `.spec.completions` 个 Pod 成功结束。

* 并行任务：设置 `.spec.Parallelism` 参数，但不设置 `.spec.completions` 参数。当所有 Pods 结束并且至少有一个 Pod 成功时，任务即成功。

{{%children style="card" description="true" %}}

