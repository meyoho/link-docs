+++
title = "计算"
description = "计算支持创建 Deployment、DaemonSet 和 StatefulSet，并支持管理 Pods。"
weight = 2
+++

计算支持：

* 创建 Deployment，用于部署对网络存储状态没有特殊要求的无状态工作负载，为 Pod 和 ReplicaSets 提供声明式更新

* 创建 DaemonSet，用来部署集群的日志、监控或系统管理应用，在每个可用的计算节点上都运行一个容器副本

* 创建 StatefulSet，适用于对网络存储状态有特殊要求的工作负载，为 Pod 提供唯一标识，保证部署和扩缩容的顺序

* 创建 ReplicaSet，确保容器应用的副本数始终保持在定义的副本数

* 创建 CronJob，用来在指定的时间周期内运行指定的任务

* 创建 Jobs，用来批量处理短暂的一次性任务

* 管理 Pods

本模块包括以下主要功能：

* [部署]({{< relref "workloads/deployment/_index.md" >}})

* [守护进程集]({{< relref "workloads/daemonset/_index.md" >}})

* ​[有状态副本集]({{< relref "workloads/stateful/_index.md" >}})

* [副本集]({{< relref "workloads/replicaset/_index.md" >}})

* [定时任务]({{< relref "workloads/cronjob/_index.md" >}})

* [任务]({{< relref "workloads/job/_index.md" >}})

* ​[容器组]({{< relref "workloads/pod/_index.md" >}})
