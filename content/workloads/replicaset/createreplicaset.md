+++
title = "创建副本集"
Description = "创建 ReplicaSet 资源，确保容器应用的副本数始终保持在定义的副本数。"
weight = 1
+++

创建 ReplicaSet 资源，确保容器应用的副本数始终保持在定义的副本数。ReplicaSet 可以单独使用，但建议使用 Deployment 来自动管理 ReplicaSet。

**操作步骤**

1. 登录 Kubernetes 平台，单击 **计算** > **副本集**。

2. ​在副本集页面，单击 **创建副本集**。

3. 在创建页面，在 **YAML (读写)** 区域，输入创建副本集的 YAML 内容。  
	* 单击 **YAML 样例** 区域的 **写入**，使用提供的 YAML 样例创建副本集。    
		* 单击 **导入**，选择已有的 YAML 文件内容作为编辑内容。

		* 单击 **导出**，将 YAML 导出保存成一个文件。 

		* 单击 **清空**，清空所有编辑内容。
		
		* 单击 **查找**，在框中输入关键字，会自动匹配搜索结果，并支持前后搜索查看。  
		
		* 单击 **复制**，复制编辑内容。

		* 单击 **日间** 或 **夜间** 模式，屏幕会自动调节成对应的查看模式。 
		* 单击 **全屏**，全屏查看内容；单击 **退出全屏**，退出全屏模式。 
	
	* 单击 **YAML 样例** 区域的 **查看**，在 **YAML 样例** 窗口，查看提供的 YAML 样例的具体内容： 

		* 单击 **导出**，将 YAML 导出保存成一个文件。
	
		* 单击 **查找**，在框中输入关键字，会自动匹配搜索结果，并支持前后搜索查看。  
		
		* 单击 **复制**，复制编辑内容。  
		
		* 单击 **日间** 或 **夜间** 模式，屏幕会自动调节成对应的查看模式。 
		 
		* 单击 **全屏**，全屏查看内容；单击 **退出全屏**，退出全屏模式。
4. 创建副本集的 YAML 内容编辑好后，单击 **创建**。  
创建副本集后，返回到 **副本集** 的列表页面，支持按名称搜索已创建的副本集。