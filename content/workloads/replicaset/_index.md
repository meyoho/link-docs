+++
title = "副本集"
Description = "使用 ReplicaSet 资源，确保容器应用的副本数始终保持在定义的副本数。ReplicaSet 可以单独使用，但建议使用 Deployment 来自动管理 ReplicaSet。"
weight = 4
+++

ReplicaSet 用来确保容器应用的副本数始终保持在定义的副本数，即如果有容器异常退出，会自动创建新的 Pod 来替代；多出来的容器也会被自动回收。ReplicaSet 支持等式和集合式的 Selector。

ReplicaSet 可以单独使用，但建议使用 Deployment 来自动管理 ReplicaSet。例如：Deployment 支持 rolling-update，
ReplicaSet 则不支持。

副本集支持的功能：

* [创建副本集]({{< relref "workloads/replicaset/createreplicaset.md" >}})

* [查看副本集详情]({{< relref "workloads/replicaset/viewreplicaset.md" >}})

* [更新副本集]({{< relref "workloads/replicaset/update.md" >}})

* [扩缩容]({{< relref "workloads/replicaset/scale.md" >}})

* [查看日志]({{< relref "workloads/replicaset/viewlog.md" >}})

* [更新标签]({{< relref "workloads/replicaset/updatelabel.md" >}})

* [更新注解]({{< relref "workloads/replicaset/updateannotation.md" >}})

* ​[使用 exec 命令]({{< relref "workloads/replicaset/exec.md" >}})

* [删除副本集]({{< relref "workloads/replicaset/delete.md" >}})
