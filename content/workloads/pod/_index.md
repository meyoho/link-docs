+++
title = "容器组"
Description = "使用 Kubernetes 中最小的管理单位 Pod 资源。Pod 包含一个或一组容器，这些容器共享网络、存储等资源。"
weight = 6
+++

Pod 包含一个或一组容器，这些容器共享网络、存储等资源。Pod 是 Kubernetes 中最小的管理单位。一个 Pod 可以包含一个或多个紧密相连的应用，这些应用统一调度在同一个物理主机或虚拟机上，共享 Pod 资源。

Pod 和包含的容器一样，并不是永久存在的资源，会随着部署主机节点的删除而删除。

{{%children style="card" description="true" %}}
