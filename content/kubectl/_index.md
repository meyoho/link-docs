+++
title = "使用 kubectl 命令行工具"
description = "进入内置的命令行工具页面，使用 kubectl 命令查看、管理 Kubernetes 资源。"
weight = 9
+++

kubectl 是一种用于运行 Kubernetes 集群命令的管理工具，进入平台内置的 kube-shell 命令行工具页面，使用 kubectl 命令查看、管理 Kubernetes 资源。本功能适用于熟悉 kubectl 命令操作的用户，执行所需的功能操作，提供较好的易用性体验。

**操作步骤**

1. 登录 Kubernetes 平台，单击页面右上方的 ![operations](/img/kubectl.png)。

2. 在 kubectl 命令行工具页面，输入要执行的 kubectl 命令。同时支持以下操作。

	* 按 **F5** 键，切换执行 kubectl 命令的命名空间。
	
	* 按 **F10** 键，可断开工具连接。

3. 关于工具的更多操作说明，参考 [kube-shell 工具文档](https://github.com/cloudnativelabs/kube-shell/blob/master/README.rst)。
	
	