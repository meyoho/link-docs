+++
title = "配置"
description = "配置支持创建 ConfigMap，实现对容器中应用的配置管理；支持创建 secret，保存敏感信息，使保存数据更安全灵活。"
weight = 3
+++

配置支持：

* 创建配置字典 (ConfigMap)，实现对容器中应用的配置管理

* 创建保密字典 (Secret)，保存敏感信息，使保存数据更安全灵活

本模块包括以下主要功能：

* [配置字典]({{< relref "config/configmaps/_index.md" >}})

* [保密字典]({{< relref "config/secrets/_index.md" >}})
