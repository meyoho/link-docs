+++
title = "保密字典"
Description = "使用 secret 保存敏感信息，例如：密码、token、 ssh key 等，保存数据更安全灵活，使数据免于暴露到镜像或 Pod Spec 中。"
weight = 2
+++

使用 Secret 保存敏感信息，例如：密码、token、ssh key 等，保存数据更安全灵活，使数据免于暴露到镜像或 Pod Spec 中。支持用环境变量或挂载存储卷的形式引用 Secret。

Secret 支持以下几种类型：

* 镜像服务：用于存储私有 Docker registry 的认证信息。例如创建一个 dockercfg 类型的 Secret，用于下载 Docker 镜像认证；或创建一个 .json 文件保存鉴权串的 dockerconfigjson 类型。

* Opaque：用于存储密码、密钥等。Opaque 类型的数据是一个 map 类型的 base64 编码格式。

* TLS 认证：TLS 类型，保存 TLS 协议证书等认证信息。

* 基本认证：basic-auth 类型，通过用户名和密码验证。

* SSH 请求认证：ssh-auth 类型，通过 SSH 协议传输数据。

{{%children style="card" description="true" %}}

