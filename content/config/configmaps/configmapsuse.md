+++
title = "使用配置字典"
Description = "创建配置后，在服务中使用 ConfigMap。"
weight = 3
+++

创建配置后，在容器中使用 ConfigMap。支持环境变量引用完整的 ConfigMap 文件，或引用 ConfigMap 文件中的部分 key；同时支持挂载文件或目录引用完整的 ConfigMap 文件，或引用 ConfigMap 文件中的部分 key 作为文件。

**操作步骤**

在每个容器中，可以通过两种方式使用 ConfigMaps：

- 设置环境变量：把 ConfigMap 中配置项的值，作为环境变量的值。支持环境变量引用完整的 ConfigMap 文件，或引用 ConfigMap 文件中的部分 key。

  * 环境变量引用完整的 ConfigMap 文件样例。  
    配置后，从 YAML 中可以看到，容器中已经引用了名称为 `configmaptest` 的配置。

    ``````yaml
    ......   
          containers:
            - name: servicedoc-0
              image: 'index.alauda.cn/productdemo/dockerdemo:latest'
              envFrom:
                - configMapRef:
                    name: configmaptest
    ...... 
    ``````

  * 环境变量引用 ConfigMap 文件中的部分 key 样例。  
    配置后，从 YAML 中可以看到，容器中已经引用了名称为 `configmaptest` 配置文件中的 key1。

    ``````yaml
    ......
          containers:
            - name: servicedoc-0
              image: 'index.alauda.cn/productdemo/dockerdemo:latest'
              env:       
                - name: fromconfigmap          
                  valueFrom:            
                    configMapKeyRef:              
                      name: configmaptest              
                      key: key1
    ......
    ``````

- 挂载文件或目录：设置以文件的方式挂载到容器内部指定路径下的文件中。支持挂载文件或目录引用完整的 ConfigMap 文件，或引用 ConfigMap 文件中的部分 key 作为文件。

  * 挂载文件或目录引用完整的 ConfigMap 文件样例。  
    配置后，从 YAML 中可以看到，容器中已经把名称为 `configmap` 的配置，以文件的形式挂载到容器内部指定路径 `/consul/configmap/` 下的 `configmap-volume` 的 volume 中。

    ``````yaml
    ......   
          containers:
            - name: servicedoc-0
              image: 'index.alauda.cn/productdemo/dockerdemo:latest'
              volumeMounts:
              - mountPath: /consul/configmap/
                name: configmap-volume
          volumes:
            - name: configmap-volume
              configMap:
                name: configmap 
    ......   
    ``````

  * 挂载文件或目录引用 ConfigMap 文件中的部分 key 样例。  
    配置后，从 YAML 中可以看到，容器中已经把 `key1` 以文件的形式挂载到容器内部指定路径 `/consul/configmap/` 下的 `configmap-key` 的 volume 中。

    ``````yaml
    ......
          containers:
            - name: servicedoc-0
            image: 'index.alauda.cn/productdemo/dockerdemo:latest'
            volumeMounts:
            - mountPath: /consul/configmap-key/
              name: configmap-key
          volumes:
            - name: configmap-key
            configMap:
              name: configmaptest
              items:           
              - key: key1            
                path: keys/key.value
    ......
    ``````

**注意**：Pod 只能使用同一个命名空间内的 ConfigMaps，不支持跨命名空间去调用。
