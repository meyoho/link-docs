+++
title = "删除配置字典"
Description = "删除已创建的配置字典。"
weight = 7
+++

删除已创建的配置字典。

**操作步骤**

1. 登录 Kubernetes 平台，单击 **配置** > **配置字典**。

2. 在配置字典列表页面，选择配置字典所在的命名空间。

3. 在配置字典列表页面，可以通过以下两种方式删除配置字典：  
	* 找到要删除的配置字典名称，单击 ![operations](/img/operations.png)，再单击 **删除**。

	* 单击要删除的 ***配置字典名称***，在详情页面，单击 **操作** > **删除**。
4. 在新窗口，单击 **确定**。