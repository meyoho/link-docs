+++
title = "其他资源"
description = "其他资源支持显示 Alauda Kubernetes 暂时未分类到具体功能模块的 Kubernetes 资源"
weight = 7
+++

其他资源支持显示 Alauda Kubernetes 暂时未分类到具体功能模块的 Kubernetes 资源，包括以下两种。

* 创建时指定了命名空间的命名空间相关资源。  

* 创建时未指定命名空间而与整个 Kubernetes 集群相关的集群相关资源。

本模块包括以下主要功能：

* [命名空间相关资源]({{< relref "others/namespacerelated/_index.md" >}})

* [集群相关资源]({{< relref "others/clusterrelated/_index.md" >}})