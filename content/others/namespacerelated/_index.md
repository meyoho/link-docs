+++
title = "命名空间相关资源"
Description = "命名空间相关资源为 Alauda Kubernetes 暂时未分类到具体功能模块的 Kubernetes 资源中，与指定命名空间相关的 Kubernetes 资源。"
weight = 1
+++

命名空间相关资源为 Alauda Kubernetes 暂时未分类到具体功能模块的 Kubernetes 资源中，与指定命名空间相关的 Kubernetes 资源。

{{%children style="card" description="true" %}}

