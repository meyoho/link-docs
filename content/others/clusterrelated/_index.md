+++
title = "集群相关资源"
Description = "集群相关资源为 Alauda Kubernetes 暂时未分类到具体功能模块的 Kubernetes 资源中，未指定命名空间而与整个 Kubernetes 集群相关的资源。"
weight = 2
+++

集群相关资源为 Alauda Kubernetes 暂时未分类到具体功能模块的 Kubernetes 资源中，未指定命名空间而与整个 Kubernetes 集群相关的资源。

{{%children style="card" description="true" %}}

