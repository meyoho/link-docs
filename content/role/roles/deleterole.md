+++
title = "删除权限规则"
Description = "删除已创建的权限规则。"
weight = 6
+++

删除已创建的权限规则。

**操作步骤**

1. 登录 Kubernetes 平台，单击 **权限管理** > **角色管理**。

2. 在角色列表页面，单击要删除权限规则的 ***角色名称***。

3. 在角色详情页面，在 **权限规则** 区域，找到要删除的权限，单击 ![operations](/img/operations.png)，再单击 **删除**。

3. 在新窗口，确认要删除的权限规则后，单击 **确定**。  
在角色详情页面，权限规则已删除。