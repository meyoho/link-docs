+++
title = "角色管理"
description = "通过对角色的管理，允许角色有相应的权限，实现对平台中资源的控制。"
weight = 1
+++

通过对角色的管理，允许角色有相应的权限，实现对平台中资源的控制。角色按照命名空间或集群的类型来分配相应的权限，实现对平台资源的控制。

一个类型的角色只能在限定的命名空间或集群中，访问可操作的资源。

角色绑定(Rolebinding)将角色（role）与用户（user）联系起来，角色绑定包含了一系列的子对象（users,groups,或者 service accounts）,然后将这些对象连接到一个拥有某种授权的角色(role)上，Rolebinding 绑定某个具体namespace的角色（Role）， ClusterRoleBinding，绑定 ClusterRole 类型的角色

{{%children style="card" description="true" %}}
