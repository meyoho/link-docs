+++
title = "退出登录"
description = "从 Kubernetes 平台上退出登录。"
weight = 6
+++

从 Kubernetes 平台上退出登录。

**操作步骤**

1. 登录 Kubernetes 平台后，如果需要退出登录，单击右上角的 ***账号名称***。

2. 单击 **退出登录**。