+++
title = "使用 OIDC 认证账号登录"
description = "使用 OIDC 认证账号登录 Kubernetes 平台。"
weight = 3
+++

如果管理员将 OIDC 配置为用户登录认证方式，普通用户应使用 OIDC 认证账号登录 Kubernetes 平台。

**操作步骤**

1. 在浏览器中打开 Alauda Kubernetes 平台。

2. 在登录界面单击 OIDC 服务的登录入口。

3. 跳转至 OIDC 服务的认证界面进行认证。

4. 认证通过后自动返回 Alauda Kubernetes 平台。