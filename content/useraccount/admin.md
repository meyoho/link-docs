+++
title = "使用管理员账号登录"
description = "使用管理员账号登录 Kubernetes 平台。"
weight = 1
+++

管理员通过邮箱和密码登录 Kubernetes 平台。

**操作步骤**

1. 在浏览器中打开 Alauda Kubernetes 平台。

2. 在登录界面中，单击 **管理员登录**。

3. 在 **email address** 框中，输入邮箱账号。

4. 在 **password** 框中，输入密码。

5. 单击 **登录**。