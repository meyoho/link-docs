+++
title = "登录管理"
description = ""
weight = 2
+++

Alauda Kubernetes 支持多种登录认证方式，可以通过以下三种类型的账号登录平台：

- [使用管理员账号登录]({{< relref "useraccount/admin.md" >}})

- [使用 LDAP 认证账号登录]({{< relref "useraccount/ldap.md" >}})

- [使用 OIDC 认证账号登录]({{< relref "useraccount/oidc.md" >}})

同时支持以下功能：

- [切换中英文显示]({{< relref "useraccount/changelanguage.md" >}})

- [平台中心]({{< relref "useraccount/portal.md" >}})

- [退出登录]({{< relref "useraccount/logout.md" >}})