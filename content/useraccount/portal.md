+++
title = "平台中心"
description = "从 Kubernetes 平台上转换到 Container Platform，Container Platform 提供了一站式的容器平台，例如 Alauda DevOps、第三方工具平台等。"
weight = 5
+++

从 Kubernetes 平台上转换到 Container Platform，Container Platform 提供了一站式的容器平台，例如 Alauda DevOps、第三方工具平台等。根据不同的用户权限，展示的内容会不同。

**操作步骤**

1. 登录 Kubernetes 平台后，如果需要转换到 Container Platform，单击右上角的 ***账号名称*** > **平台中心**。

2. 在 **平台中心** 窗口，查看一站式容器平台提供的平台功能：

	* Alauda 平台：提供了一站式的容器平台体验。选择要使用的平台后，单击 **进入平台**，进入到具体的平台。

		* Alauda Kubernetes：Alauda Kubernetes 是一个 Kubernetes 商业化工具平台，提供了一键部署 Kubernetes，同时解决 Kubernetes 使用难等问题。

		* ACP DevOps：Alauda Container Platform DevOps 是一个基于容器的 DevOps 研发云应用平台。平台为企业提供包含需求管理、项目管理、研发、测试、运维等服务在内的开箱即用一站式服务。

		* ACP Machine Learning：Alauda Container Platform Machine Learning 是一套聚焦模型训练与发布模型服务的机器学习系统，可以进一步降低 AI 使用门槛，降低企业技术风险。

		* ACP Service Framework：Alauda Container Platform Service Framework 是一个分布式服务框架平台。基于 Spring Cloud 分布式服务框架，支持分布式服务发布与注册、服务调用、服务鉴权、服务降级、服务限流、配置管理、调用链跟踪等功能。

	* 第三方工具平台：配置了专属的第三方工具平台，提供更便捷的访问方式。单击工具平台的图标后，进入到具体的平台。

		* 代码工具：提供了 GitHub、Bitbucket、GitLab、码云等代码仓库，还有代码质量管理工具 SonarQube 等。
	
		* 运维管理：提供了 Kibana 日志、Grafana 监控面板。
	
		* 项目管理：提供了项目与事物追踪工具 Jira、管理与协同软件 Confluence、禅道、Trello 看板。