+++
title = "Alauda Kubernetes"
description = ""
+++

# Alauda Kubernetes

Alauda Kubernetes 基于 Kubernetes 原生的 API，让开发者像管理产品一样管理工作负载，同时免去了大量的学习成本。

Alauda Kubernetes 由安装器和发行版 UI 组成，轻松解决安装部署问题，更好的发挥 Kubernetes 特性。

* 安装器：提供了一种更快速的 Kubernetes 部署方式，可实现一键部署 Kubernetes 集群。

* 发行版 UI：提供了简洁的图形化界面，为容器化应用提供资源调度、部署运行、服务发现、扩容缩容等一系列功能。

使用 Alauda Kubernetes，有助于：

* 提高运维能力，实现快速响应

* 提高资源的利用率，使工作负载管理更轻便

* 接入 Alauda Container Platform (ACP)，例如：Alauda DevOps、微服务等，实现流程最大程度的智能化

* 简化学习成本，提升组织工作效率

* 角色清晰，保证资源隔离

Alauda Kubernetes 包括以下主要功能：

* [概览]({{< relref "overview/_index.md" >}})

* [计算]({{< relref "workloads/_index.md" >}})

* ​[配置]({{< relref "config/_index.md" >}})

* [网络]({{< relref "network/_index.md" >}})

* [存储]({{< relref "storage/_index.md" >}})

* [集群]({{< relref "clusters/_index.md" >}})

* [其他资源]({{< relref "others/_index.md" >}})
